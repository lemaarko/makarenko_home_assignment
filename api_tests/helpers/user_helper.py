import time
import allure
from jsonschema import validate

from api_tests.helpers.api_client import ApiClient
from settings import base_api_address


def create_user():
    payload = {
        "id": 0,
        "username": str(int(time.time())),
        "firstName": "Test",
        "lastName": "User",
        "email": "otutoso+1@gmail.com",
        "password": "Qwerty1!",
        "phone": "+380637819993",
        "userStatus": 0
    }
    api_client = ApiClient(base_api_address)
    request = api_client.post(path='user', json=payload)
    assert request.status_code == 200
    return payload


def validate_schema(response, expected_schema):
    with allure.step(f"Checking response {response}"):
        validate(response, expected_schema)
