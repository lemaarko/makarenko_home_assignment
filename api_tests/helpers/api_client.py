import requests
import allure


class ApiClient:
    def __init__(self, base_api_address):
        self.base_address = base_api_address
        self.default_headers = {'accept': 'application/json', 'Content-Type': 'application/json',
                                'api_key': 'special-key'}

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        headers = headers or self.default_headers
        with allure.step(f'POST request to: {url}'):
            return requests.post(url=url, params=params, data=data, json=json, headers=headers)

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        headers = headers or self.default_headers
        with allure.step(f'GET request to: {url}'):
            return requests.get(url=url, params=params, headers=headers)

    def put(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        headers = headers or self.default_headers
        with allure.step(f'POST request to: {url}'):
            return requests.put(url=url, params=params, data=data, json=json, headers=headers)

    def delete(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        headers = headers or self.default_headers
        with allure.step(f'GET request to: {url}'):
            return requests.delete(url=url, params=params, headers=headers)
