error_schema = {
    "type": "object",
    "properties": {
        "code": {"type": "number"},
        "type": {"type": "string"},
        "message": {"type": "string"},
    },
}

user_profile = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "username": {"type": "string"},
        "firstName": {"type": "string"},
        "lastName": {"type": "string"},
        "email": {"type": "string"},
        "password": {"type": "string"},
        "phone": {"type": "string"},
        "userStatus": {"type": "number"}
    },
    "required": ["id", "username", "firstName", "lastName", "email", "password", "phone", "userStatus"],
    "additionalProperties": False
}
