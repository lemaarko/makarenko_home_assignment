import allure
import pytest

from api_tests.response_schema.user import error_schema
from api_tests.helpers.user_helper import validate_schema

payload = {
    "id": 0,
    "username": "username",
    "firstName": "Test",
    "lastName": "User",
    "email": "otutoso+1@gmail.com",
    "password": "Qwerty1!",
    "phone": "+380637819993",
    "userStatus": 0
}
payload2 = {
    "id": 0,
    "username": "username",
    "firstName": "Test",
    "lastName": "User",
    "email": "otutoso+1@gmail.com",
    "password": "Qwerty1!",
    "phone": "+380637819993",
    "userStatus": 1233476587638426
}

test_data = (
    ['Valid data.', payload, 'username', 200, error_schema],
    ['Invalid userStatus.', payload2, 'username', 500, error_schema]
)


@allure.feature('Update user')
@allure.link('https://petstore.swagger.io/#/user/updateUser', name='Update user')
@pytest.mark.parametrize('payload, username, expected_status_code, expected_schema', test_data,
                         ids=[i.pop(0) for i in test_data])
def test_update_user(payload, username, expected_status_code, expected_schema, pet_store_api):
    response = pet_store_api.put(path=f'user/{username}', json=payload)
    validate_schema(response.json(), expected_schema)
    assert response.status_code == expected_status_code
