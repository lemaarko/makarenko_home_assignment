import allure

from api_tests.response_schema.user import user_profile, error_schema
from api_tests.helpers.user_helper import validate_schema


@allure.feature('Get user')
@allure.link('https://petstore.swagger.io/#/user/getUserByName', name='Get user')
def test_get_user_with_valid_username(pet_store_api, new_user):
    username = new_user.get('username')
    response = pet_store_api.get(path=f'user/{username}')
    validate_schema(response.json(), user_profile)
    assert response.status_code == 200


@allure.feature('Get user')
@allure.link('https://petstore.swagger.io/#/user/getUserByName', name='Get user')
def test_get_user_with_invalid_username(pet_store_api):
    invalid_username = '!@$'
    response = pet_store_api.get(path=f'user/{invalid_username}')
    validate_schema(response.json(), error_schema)
    assert response.status_code == 404
