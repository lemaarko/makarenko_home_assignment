import allure

from api_tests.response_schema.user import error_schema
from api_tests.helpers.user_helper import validate_schema


@allure.feature('Delete user')
@allure.link('https://petstore.swagger.io/#/user/deleteUser', name='Delete user')
def test_delete_existed_user(pet_store_api, new_user):
    username = new_user.get('username')
    response = pet_store_api.delete(path=f'user/{username}')
    with allure.step(f"Checking response {response.json()}"):
        validate_schema(response.json(), error_schema)
    with allure.step(f"Checking status_code {response.status_code}"):
        assert response.status_code == 200


@allure.feature('Delete user')
@allure.link('https://petstore.swagger.io/#/user/deleteUser', name='Delete user')
def test_delete_not_existed_user(pet_store_api):
    not_existed_user = '---------'
    response = pet_store_api.delete(path=f'user/{not_existed_user}')
    assert response.status_code == 404
