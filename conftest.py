import time
import logging
import pytest
import allure
from allure_commons.types import AttachmentType

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from settings import web_site_address, base_api_address
from api_tests.helpers.api_client import ApiClient
from api_tests.helpers.user_helper import create_user


@pytest.fixture
def new_user(pet_store_api):
    yield create_user()


def make_screenshot(driver, test_name):
    name = f'{test_name}{int(time.time())}.png'
    allure.attach(driver.get_screenshot_as_png(), name=name, attachment_type=AttachmentType.PNG)


def check_browser_console(driver, title):
    browser_logs = driver.get_log('browser')
    if browser_logs:
        logging.warning('\n')
        logging.critical(f'----- {title} CONSOLE LOGS START -----')
        for logs in browser_logs:
            logging.warning(logs.get('message', logging))
        logging.critical(f'----- {title} CONSOLE LOGS END -----')
    logging.warning('\n')


@pytest.fixture()
def browser(request):
    chrome_options = Options()
    chrome_options.add_argument("--window-size=1280,1024")
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(web_site_address)
    failed_before = request.session.testsfailed
    yield driver
    if request.session.testsfailed != failed_before:
        check_browser_console(driver, 'BROWSER')
        logging.critical(f'url = {driver.current_url}')
        make_screenshot(driver, test_name=request.node.name)
    if driver:
        driver.quit()


@pytest.fixture()
def iphone_browser(request):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation", {"deviceName": "iPhone X"})
    driver = webdriver.Chrome(options=chrome_options)
    driver.get(web_site_address)
    failed_before = request.session.testsfailed
    yield driver
    if request.session.testsfailed != failed_before:
        check_browser_console(driver, 'BROWSER')
        logging.critical(f'url = {driver.current_url}')
        make_screenshot(driver, test_name=request.node.name)
    if driver:
        driver.quit()


@pytest.fixture
def pet_store_api():
    return ApiClient(base_api_address=base_api_address)
