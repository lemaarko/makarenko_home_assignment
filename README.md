## Before run:

- Install [Python3](https://www.python.org/downloads/)
  
- Inside the project create virtual env:
  
    `python3 -m venv /path/to/new/virtual/environment`

- Activate virtual env
  
    `source venv/bin/activate`

- Check you are using venv python with command 
    
    `which python`

- Install project dependencies 
  
    `pip install -r requirements.txt`

- Download [Chromedriver](https://chromedriver.chromium.org/downloads) for your Chrome browser version
  
- Add chromedriver to the system path
  
- Install Allure for reports 

    `brew install allure`
    
## Execute tests

- To run all the tests use command inside the project
  
    `pytest --alluredir=./tmp/allure_results`
  
## Check test report

- Run command to see the allure report

    `allure serve ./tmp/allure_results`