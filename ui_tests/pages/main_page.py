import allure
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException
from settings import web_site_address
from ui_tests.pages.base_page import BasePage
from ui_tests.locators.main_page_locators import PLATFORM_MENU_ITEM, SOLUTIONS_MENU_ITEM, ABOUT_US_MENU_ITEM, \
    CONTACT_US_MENU_ITEM, BLOG_MENU_ITEM, CASE_STUDIES_MENU_ITEM, FOOTER_CONTACT_US_EMAIL_INPUT, \
    FOOTER_CONTACT_US_FIRSTNAME_INPUT, FOOTER_CONTACT_US_SUBMIT, BODY_CONTACT_US_FORM_SUBMIT, \
    BODY_CONTACT_US_FORM_EMAIL, BODY_CONTACT_US_FORM_FIRSTNAME, SEARCH_ICON, SEARCH_INPUT, SUBMIT_SEARCH_BUTTON, \
    SEARCH_NEXT_PAGE, SEARCH_CONTENT_TITLE, SEARCH_CONTENT_DESC, SEARCH_RESULT_TITLE


class MainPage(BasePage):
    def open_main_page(self):
        with allure.step(f'Opening page: {web_site_address}'):
            self.driver.get(web_site_address)

    def get_top_menu_items(self):
        menu_elements = {'platform': PLATFORM_MENU_ITEM,
                         'solutions': SOLUTIONS_MENU_ITEM,
                         'about_us': ABOUT_US_MENU_ITEM,
                         'contact_us': CONTACT_US_MENU_ITEM,
                         'blog': BLOG_MENU_ITEM,
                         'case_studies': CASE_STUDIES_MENU_ITEM}
        result = []
        with allure.step('Getting menu items.'):
            for name, selector in menu_elements.items():
                element = self.driver.find_element(*selector)
                if element.is_displayed():
                    result.append(name)
        with allure.step(f'Menu items: {result}'):
            return result

    def is_contact_form_displayed_in_footer(self):
        firstname_input = self.driver.find_elements(*FOOTER_CONTACT_US_FIRSTNAME_INPUT)
        email_input = self.driver.find_elements(*FOOTER_CONTACT_US_EMAIL_INPUT)
        submit_btn = self.driver.find_elements(*FOOTER_CONTACT_US_SUBMIT)
        with allure.step('Checking if contact form displayed in footer.'):
            return True if firstname_input and email_input and submit_btn else False

    def is_contact_form_displayed_in_body(self):
        firstname_input = self.driver.find_elements(*BODY_CONTACT_US_FORM_SUBMIT)
        email_input = self.driver.find_elements(*BODY_CONTACT_US_FORM_FIRSTNAME)
        submit_btn = self.driver.find_elements(*BODY_CONTACT_US_FORM_EMAIL)
        with allure.step('Checking if contact form displayed in body.'):
            return True if firstname_input and email_input and submit_btn else False

    def open_search_window(self):
        with allure.step('Clicking on search icon'):
            self.wait(self.driver).until(ec.element_to_be_clickable(SEARCH_ICON)).click()

    def enter_search_query(self, text: str):
        with allure.step('Filling search input'):
            search_input = self.wait(self.driver).until(ec.element_to_be_clickable(SEARCH_INPUT))
            search_input.click()
            search_input.send_keys(text)

    def submit_search(self):
        with allure.step('Submit search'):
            submit_btn = self.wait(self.driver).until(ec.element_to_be_clickable(SUBMIT_SEARCH_BUTTON))
            submit_btn.click()
            self.wait(self.driver).until(ec.staleness_of(submit_btn))
            self.wait(self.driver).until(ec.presence_of_element_located(SEARCH_RESULT_TITLE))

    def search_on_the_site(self, query_to_search: str):
        self.open_search_window()
        self.enter_search_query(query_to_search)
        self.submit_search()

    def get_search_content(self):
        titles = self.wait(self.driver).until(ec.presence_of_all_elements_located(SEARCH_CONTENT_TITLE))
        descriptions = self.driver.find_elements(*SEARCH_CONTENT_DESC)
        with allure.step('Getting search result'):
            return tuple(zip([t.text for t in titles], [d.text for d in descriptions]))

    def open_pagination_page_by_number(self, number_of_page: int):
        if number_of_page > 1:
            for number in range(1, number_of_page):
                try:
                    with allure.step('Clicking next pagination page.'):
                        next_btn = self.wait(self.driver, 3).until(ec.element_to_be_clickable(SEARCH_NEXT_PAGE))
                        self.scroll_page_and_click(next_btn)
                except TimeoutException:
                    with allure.step(f'Last page with content is {number}.'):
                        pass
