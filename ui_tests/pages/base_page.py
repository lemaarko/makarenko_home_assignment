import time
import allure
from allure_commons.types import AttachmentType
from selenium.webdriver.support.ui import WebDriverWait
from ui_tests.locators.main_page_locators import LOGO, ACCEPT_COOKIES_BUTTON, CLOSE_MODAL_BUTTON
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import ElementClickInterceptedException, StaleElementReferenceException


class BasePage:
    def __init__(self, driver):
        self.driver = driver
        self.accept_cookies()
        self.close_modal_window()

    @staticmethod
    def wait(driver, ttl=10):
        """
        Base method to wait expected conditions.
        :param driver: instance of browser
        :param ttl: time to wait
        :return: WebDriverWait object
        """
        return WebDriverWait(driver, ttl)

    def is_logo_displayed(self):
        logo_elements = self.driver.find_elements(*LOGO)
        with allure.step(f'Is logo displayed: {bool(logo_elements)}'):
            return bool(logo_elements)

    def accept_cookies(self):
        with allure.step('Accepting cookies.'):
            self.wait(self.driver).until(ec.presence_of_element_located(ACCEPT_COOKIES_BUTTON)).click()

    def close_modal_window(self):
        with allure.step('Closing modal window before tests.'):
            self.wait(self.driver).until(ec.presence_of_element_located(CLOSE_MODAL_BUTTON)).click()

    def move_to_element(self, element):
        """
        find element in dom and move to it on UI
        """
        action = ActionChains(self.driver)
        with allure.step('Scrolling page into element view.'):
            action.move_to_element(element).perform()

    def scroll_page_and_click(self, element):
        """
        Tries to click the element, if not scrolls down/up and tries to click again.

        :param element: selenium element
        :return: None
        """
        try:
            self.move_to_element(element)
            element.click()
        except (ElementClickInterceptedException, StaleElementReferenceException):
            self.driver.execute_script("window.scrollBy(0, 300);")
            time.sleep(0.5)
            element.click()

    def make_screenshot(self):
        name = f'screenshot{int(time.time())}.png'
        with allure.step('Saving screenshot.'):
            allure.attach(self.driver.get_screenshot_as_png(), name=name, attachment_type=AttachmentType.PNG)
