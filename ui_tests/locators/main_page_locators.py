from selenium.webdriver.common.by import By

LOGO = (By.CSS_SELECTOR, '.hs_cos_wrapper_type_logo[data-hs-cos-type="logo"]')

SEARCH_ICON = (By.CSS_SELECTOR, '.search')
SEARCH_INPUT = (By.CSS_SELECTOR, 'input[type="search"]')
SUBMIT_SEARCH_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"]')

PLATFORM_MENU_ITEM = (By.XPATH, '//a[@href="https://web.cloudmore.com/product"]')
SOLUTIONS_MENU_ITEM = (By.XPATH, '//a[@href="https://web.cloudmore.com/solutions"]')
ABOUT_US_MENU_ITEM = (By.XPATH, '//a[@href="https://web.cloudmore.com/about-us"]')
CONTACT_US_MENU_ITEM = (By.XPATH, '//a[@href="https://web.cloudmore.com/contact-us"]')
BLOG_MENU_ITEM = (By.XPATH, '//a[@href="https://web.cloudmore.com/blog"]')
CASE_STUDIES_MENU_ITEM = (By.XPATH, '//a[@href="https://web.cloudmore.com/case-studies"]')

CLOSE_MODAL_BUTTON = (By.CSS_SELECTOR, 'button[aria-label="Close"]')
ACCEPT_COOKIES_BUTTON = (By.CSS_SELECTOR, 'a#hs-eu-confirmation-button[role="button"]')

FOOTER_CONTACT_US_FIRSTNAME_INPUT = (By.CSS_SELECTOR, '#contact input[name="firstname"]')
FOOTER_CONTACT_US_EMAIL_INPUT = (By.CSS_SELECTOR, '#contact input[name="email"]')
FOOTER_CONTACT_US_SUBMIT = (By.CSS_SELECTOR, '#contact input[type="submit"]')

BODY_CONTACT_US_FORM_FIRSTNAME = (By.XPATH, '//*[contains(@class,"body-container")]//input[@name="firstname"]')
BODY_CONTACT_US_FORM_EMAIL = (By.XPATH, '//*[contains(@class,"body-container")]//input[@name="email"]')
BODY_CONTACT_US_FORM_SUBMIT = (By.XPATH, '//*[contains(@class,"body-container")]//input[@type="submit"]')

SEARCH_RESULT_TITLE = (By.CSS_SELECTOR, '.search-results-title')
SEARCH_NEXT_PAGE = (By.CSS_SELECTOR, '.hs-search-results__next-page')
SEARCH_CONTENT_TITLE = (By.CSS_SELECTOR, '.hs-search-results__title')
SEARCH_CONTENT_DESC = (By.CSS_SELECTOR, '.hs-search-results__description')
