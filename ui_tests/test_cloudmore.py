from ui_tests.pages.main_page import MainPage
from settings import web_site_address


def test_main_page(browser):
    page = MainPage(browser)
    logo = page.is_logo_displayed()
    assert logo, 'Logo is not displayed'
    menu_items = page.get_top_menu_items()
    assert menu_items == ['platform', 'solutions', 'about_us', 'contact_us', 'blog', 'case_studies']


def test_pages_have_logo(browser):
    pages_to_test = ['product', 'solutions', 'about-us', 'contact-us', 'blog', 'case-studies']
    pages_without_logo = []
    page = MainPage(browser)
    for page_name in pages_to_test:
        browser.get(f'{web_site_address}{page_name}')
        if not page.is_logo_displayed():
            pages_without_logo.append(page_name)
    assert not pages_without_logo, f'Logo is not displayed on page(s) {pages_without_logo}'


def test_contact_us_has_contact_us_form(browser):
    page = MainPage(browser)
    browser.get(f'{web_site_address}contact-us')
    body_form = page.is_contact_form_displayed_in_body()
    footer_form = page.is_contact_form_displayed_in_footer()
    assert body_form and footer_form, 'Contact us form is not displayed'


def test_search_on_mobile_browser(iphone_browser):
    page = MainPage(iphone_browser)
    page.search_on_the_site('Högset')
    search_result = page.get_search_content()
    assert len(search_result) == 10
    page.open_pagination_page_by_number(3)
    page.make_screenshot()


def test_search_on_desctop_browser(browser):
    page = MainPage(browser)
    page.search_on_the_site('Högset')
    search_result = page.get_search_content()
    assert len(search_result) == 10
    page.open_pagination_page_by_number(3)
    page.make_screenshot()
